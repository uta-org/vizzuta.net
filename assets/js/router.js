$(document).ready(function() {
   var parts = window.location.pathname.split("/");
   if(parts.includes("contact")) {
       // Show contact form
       var index = parts.indexOf("contact") + 1;
       var reason = index >= parts.length ? "" : parts[index];
       
       $('#contact-window').modal('show');
   }
});