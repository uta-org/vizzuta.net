if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}

$(document).ready(function(){

    var col1_data,col2_data;

    col1_data = $(".swap-col:nth-child(1)").html();
    col2_data = $(".swap-col:nth-child(2)").html();

    var w = $(window).width();        

    if (w < 768)
    swap_columns();

    function swap_columns()
    {
        var w = $(window).width();
        if (w < 768)
        {
            $(".swap-col:nth-child(2)").html(col1_data);
            $(".swap-col:nth-child(1)").html(col2_data);
        }
        else
        {
            $(".swap-col:nth-child(1)").html(col1_data);
            $(".swap-col:nth-child(2)").html(col2_data);
        }
    }


    $(window).resize(function() {
        swap_columns();
    });
    
    var arr = ["es", "fr", "de", "nl", "pt", "zh-CN", "zh-TW", "it", "ja", "id", "ko", "ru", "ca", "gl", "eu", "ro"];    

    for(var i = 0; i < arr.length; ++i) {
        var l = arr[i];
        $(".lang-menu").append('<li class="flag '+l+'"><span></span><a data-lng="'+l+'_cap" href="/'+l+'"></a></li>');
    }
    
    var lng = isLocalhost() ? getParam('lang') : window.location.pathname.split('/')[1];
    // setTimeout(function() {translate(lng, arr.indexOf(lng));}, 500);
    translate(lng, arr.indexOf(lng));
    
    $('.lang-button').on('click', function() {
        var langmenu = $('.lang-menu');
        
        if(langmenu.is(":hidden"))
            langmenu.slideDown();
        else
            langmenu.slideUp();
        
        return false;
    })
    
    $('.flag').on('click', function() {
        var lang = $(this).find('a').attr('href');
        if(isLocalhost()) {
            lang = lang.substr(1);
            lang = "/index.html?lang="+lang;
        }
        window.location = lang;
    });
    
    $('.lang-menu li:not(.flag)').on('click', function() {
        var url = $(this).find('a').attr('href');
        var win = window.open(url, '_blank');
        win.focus(); 
    });
    
    $("#contact-reason").on('change', function() {
       for(var i = 0; i < 3; ++i) {
           $("#reason_"+i).hide();
           $("#reason_"+i).find("input").removeAttr("required");
       }
       var reason = $("#reason_"+this.value);
       reason.css('display', 'flex');
       reason.find("input").attr('required', '');
    });
    
    $("#contact").on("submit", function(e) {
       e.preventDefault();
       sendForm($(this).attr("id"), $(this).serialize(), e); 
    });
});

function translate(lng, index) {
    if(lng == '') return; // Don't translate and keep in english
    
    var arrCrowdin = ["es-ES", "fr", "de", "nl", "pt-PT", "zh-CN", "zh-TW", "it", "ja", "id", "ko", "ru", "ca", "gl", "eu", "ro"];
    var langCode = arrCrowdin[index];
    
    console.log("Using language: "+lng);
    
    var fileCrowdin = "/assets/langs/crowdin/"+langCode+"/lang.csv";
    var fileUrl = doesExists(fileCrowdin) ? fileCrowdin : "/assets/langs/lang."+lng+".csv";
    
    $.get(fileUrl, function(e) {
        var lines = e.split("\n");
        // console.log(e);
        console.log("Reading "+lines.length+" lines of text!");

        for(var i = 1; i < lines.length; ++i) {
            var line = lines[i];
            if(line == '') continue;
            
            line = line.replace(/\\;/g, "/**/");
            
            var parts = line.split(";");
            var id = parts[0];
            var tr = parts[2].replace("/**/", ";");

            var txt = tr.replace('\\', '');
            txt = replace_placeholder(id, txt);

            var isHTML = RegExp.prototype.test.bind(/(<([^>]+)>)/i);

            // console.log("replacing "+id+" to "+txt);
            // console.log($("[data-lng='"+id+"']"));

            txt = txt.substring(1, txt.length - 1);
            
            if(isHTML(txt))
                $("[data-lng="+id+"]").html(txt);
            else
                $("[data-lng="+id+"]").text(txt);
        }
    });
}

function replace_placeholder(id, str) {
    switch(id) {
        case 'copyright':
            str = str.format(
                new Date().getFullYear(), // 0
                '<i class="fa fa-heart" aria-hidden="true"></i>', // 1
                '<a href="https://colorlib.com" target="_blank">Colorlib</a>', // 2
                '<a href="https://z3nth10n.net/">z3nth10n</a>' // 3
            );
            break;
    }
    
    return str;
}

function getParam(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function isLocalhost() {
    return location.hostname === "localhost" || location.hostname === "127.0.0.1" || location.hostname === "vizzuta.app";
}

// When the user scrolls the page, execute myFunction
window.onscroll = function() {updateScroll()};

function updateScroll() {
  var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
  var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
  var scrolled = (winScroll / height) * 100;
  document.getElementById("myBar").style.width = scrolled + "%";
}

function doesExists(url)
{
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status!=404;
}

function sendForm(id, data, e) {
    // alert("sending form");
    $.ajax({
        type: "POST",
        url: "https://"+window.location.hostname+"/php/forms/"+id+".form.php",
        data: data,
        dataType: "json",
        success: function(data) {
            console.log(data);
        },
        error: function() {

        }
    });
}