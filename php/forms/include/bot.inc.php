<?php

require_once __DIR__ . '/../../lib/uzphp/vendor/autoload.php';

function sendOnce($str) {
    try {
        $loop = React\EventLoop\Factory::create();
        $factory = new React\Datagram\Factory($loop);

        $factory->createClient('192.168.5.104:8090')->then(function (React\Datagram\Socket $client) use($loop) {
            $client->send($str);
            $loop->stop(); // Once sent stop loop

            /*
            $client->on('message', function($message, $serverAddress, $client) {
                echo 'received "' . $message . '" from ' . $serverAddress. PHP_EOL;
            });
            */
        });

        $loop->run();
    } catch (Exception $e) {
        die($e->getMessage());
    }
}