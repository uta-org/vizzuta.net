<?php

require __DIR__ . '/../lib/uzphp/funcs/db.funcs.php';
require __DIR__ . '/../lib/uzphp/funcs/logger.funcs.php';

$name = @$_POST["name"]; 
$email = @$_POST["email"]; 
$subject = @$_POST["subject"]; 
$reason = @$_POST["reason"]; 
$message = @$_POST["message"]; 
$age = @$_POST["age"]; 
$mobile_phone = @$_POST["mobile_phone"]; 
$about_you = @$_POST["about_you"]; 
$website = @$_POST["website"];

if(!isset($age) || empty(strlen($age)))
   $age = 'NULL';

if(!isset($name) || !isset($email) || !isset($subject) || !isset($reason))
   dieRequired();

switch($reason) {
   case "0":
      if(!isset($message)) {
         dieRequired();
      }
      break;

   case "1":
      if(!isset($age) || !isset($mobile_phone) || !isset($about_you)) {
         dieRequired();
      }
      break;

   case "2":
      if(!isset($website)) {
         dieRequired();
      }
      break;
}

// insertIntoDatabaseByData($tablename, $data, $datetime_column_name)

$data = array("name" => $name,
   "email" => $email,
   "subject" => $subject,
   "reason" => $reason,
   "message" => $message,
   "age" => $age,
   "mobile_phone" => $mobile_phone,
   "about_you" => $about_you,
   "website" => $website);

$result = insertIntoDatabaseByData("contact", $data, "created_at");
if($result === -1)
   die(json_encode(array("error" => "Error ocurred while inserting value into database!")));

// TODO: Notify bot and send email

function dieRequired() {
   die(json_encode(array("error" => "Some required fields wasn't set!")));
}