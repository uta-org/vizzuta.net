<?php

require_once __DIR__ . "/classes/Route.php";

$routes = new Route(true);
$route = $routes->getRoutes();
 
// print_r($route);

$langArr = ["es", "fr", "de", "nl", "pt", "zh-CN", "zh-TW", "it", "ja", "id", "ko", "ru", "ca", "gl", "eu", "ro"];
foreach($route as $part) {
    if(in_array($part, $langArr)) {
        require_once __DIR__. '/langs.php';
    }
}