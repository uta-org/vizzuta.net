<?php

$lang = @$_GET["lang"];
if(!isset($lang)) $lang = 'es';

$contents = file_get_contents(__DIR__.'/../assets/langs/lang.'.$lang.'.csv');
$lines = preg_split("/\r\n|\n|\r/", $contents);

$lang_data = array();
foreach($lines as $line) {
    $line = str_replace('\\;', '/**/', $line);
    $parts = explode(';', $line);
    if(!isset($parts) || empty(count($parts)) || count($parts) < 3) continue;
    
    $id = $parts[0];
    $en = str_replace('\\', '', str_replace('/**/', ';', $parts[1]));
    $tr = str_replace('\\', '', str_replace('/**/', ';', $parts[2]));
    
    $lang_data[$id] = isset($lang) ? $tr : $en;
}