<?php
   require_once __DIR__. '/php/router.php';
   require_once __DIR__. '/php/langs.php';
?>
<!doctype html>
<html class="no-js" lang="zxx">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>VIZZUTA</title>
      <meta name="description" content="<?=$lang_data['og-desc'];?>">
      <meta property="og:title" content="<?=$lang_data['og-name'];?>">
      <meta property="og:site_name" content="VIZZUTA">
      <meta property="og:description" content="<?=$lang_data['og-desc'];?>">
      <meta property="og:image" content="//vizzuta.net/assets/img/logo/full_logo.png">
      <meta property="og:url" content="https://vizzuta.net/">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="manifest" href="site.webmanifest">
      <link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicon.png">
      <!-- CSS here -->
      <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
      <link rel="stylesheet" href="/assets/css/owl.carousel.min.css">
      <link rel="stylesheet" href="/assets/css/slicknav.css">
      <link rel="stylesheet" href="/assets/css/flaticon.css">
      <link rel="stylesheet" href="/assets/css/animate.min.css">
      <link rel="stylesheet" href="/assets/css/magnific-popup.css">
      <link rel="stylesheet" href="/assets/css/fontawesome-all.min.css">
      <link rel="stylesheet" href="/assets/css/themify-icons.css">
      <link rel="stylesheet" href="/assets/css/slick.css">
      <link rel="stylesheet" href="/assets/css/nice-select.css">
      <link rel="stylesheet" href="/assets/css/style.css">
      <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
   </head>
   <body>
      <!--? Preloader Start -->
      <div id="preloader-active">
         <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
               <div class="preloader-circle"></div>
               <div class="preloader-img pere-text">
                  <img src="/assets/img/logo/loder.jpg" alt="">
               </div>
            </div>
         </div>
      </div>
      <!-- Preloader Start -->
      <header>
         <!--? Header Start -->
         <div class="header-area header-transparent">
            <div class="main-header header-sticky">
               <div class="progress-container">
                  <div class="progress-bar" id="myBar"></div>
               </div>
               <div class="container-fluid">
                  <div class="row align-items-center">
                     <!-- Logo -->
                     <div class="col-xl-2 col-lg-2 col-md-1">
                        <div class="logo">
                           <a href="/"><img src="/assets/img/logo/logo.png" alt=""></a>
                        </div>
                     </div>
                     <div class="col-xl-10 col-lg-10 col-md-10">
                        <div class="menu-main d-flex align-items-center justify-content-end">
                           <!-- Main-menu -->
                           <div class="main-menu f-right d-none d-lg-block">
                              <nav>
                                 <ul id="navigation">
                                    <li><a data-lng="home" href="/">Home</a></li>
                                    <li><a data-lng="about" href="#about">About</a></li>
                                    <li><a data-lng="profiles" href="#profiles">Profiles</a></li>
                                    <li><a data-lng="our-team" href="#team">Our Team</a></li>
                                    <li><a data-lng="join-us" href="#join-us">Join Us</a></li>
                                    <li><a data-lng="services" href="#services">Services</a></li>
                                    <!---
                                       <li><a data-lng="" href="gallery.html">Gallery</a></li>
                                       <li><a data-lng="" href="services.html">Services</a></li>
                                       <li><a data-lng="" href="blog.html">Blog</a>
                                           <ul class="submenu">
                                               <li><a href="blog.html">Blog</a></li>
                                               <li><a href="blog_details.html">Blog Details</a></li>
                                               <li><a href="elements.html">Element</a></li>
                                           </ul>
                                       </li>
                                       -->
                                    <li><a data-lng="contact" href="#" data-form="contact">Contact</a></li>
                                 </ul>
                              </nav>
                           </div>
                           <div class="header-right-btn f-right d-none d-lg-block ml-20 lang-parent">
                              <a data-lng="select-lang" href="#" class="btn header-btn lang-button">Select language</a>
                              <ul class="lang-menu" style="display: none;">
                                 <!---
                                    <li class="flag es"><span></span><a data-lng="es_cap" href="/es">SPANISH</a></li>
                                    <li class="flag us"><span></span><a data-lng="en_cap" href="/en">ENGLISH</a></li>
                                    -->
                                 <li><a data-lng="help-crowdin" href="https://crowdin.com/project/vizzutanet">Help us in Crowdin!</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <!-- Mobile Menu -->
                     <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Header End -->
      </header>
      <main>
         <!--? slider Area Start-->
         <div class="slider-area ">
            <div class="slider-active">
               <!-- Single Slider -->
               <div class="single-slider slider-height d-flex align-items-center">
                  <div class="container">
                     <div class="row">
                        <div class="col-xl-7 col-lg-7 col-md-8">
                           <div class="hero__caption">
                              <span data-lng="creative-ideas" data-animation="fadeInUp" data-delay=".4s">Creative Ideas</span>
                              <h1 data-lng="header-goal" data-animation="fadeInUp" data-delay=".6s">Our goal is to create a better world with our solutions.</h1>
                              <!-- Hero-btn -->
                              <div class="hero__btn">
                                 <a data-lng="watch-portfolio" href="#about" class="btn hero-btn"  data-animation="fadeInLeft" data-delay=".8s">Watch Portfolio</a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- Hero Man Img -->
                  <div class="hero-man">
                     <img src="/assets/img/hero/hero_man1.png" alt="">
                  </div>
               </div>
            </div>
         </div>
         <!-- slider Area End-->
         <!--? About Area start -->
         <section id="about" class="about-area about1 section-padding30">
            <div class="container">
               <div class="row justify-content-between">
                  <div class="col-xl-5 col-lg-6 swap-col">
                     <div class="about-caption2 mb-50" data-aos="fade-right">
                        <h3 data-lng="who-we-are" class="hidden-xs-down">WHO WE ARE?</h3>
                        <div class="send-cv">
                           <!--- <a href="#">hire@colorlib.com</a> -->
                           <a data-lng="who3" href="#">Our organization is shaped by specialized members in multiple areas, from software development, traditional art and conceptual, to 3D design.</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-5 col-lg-5 swap-col">
                     <div class="about-caption mb-50" data-aos="fade-left">
                        <h3 data-lng="who-we-are" class="hidden-sm-up" class="who-we-are">WHO WE ARE?</h3>
                        <h3 data-lng="who2">We are an independent organization dedicated to networking and looking into accelerating the development of the talents of its members.</h3>
                        <p class="pera1"><span>ÁLVARO (Z3NTH10N)</span> CEO</p>
                        <div class="experience">
                           <div class="year">
                              <span>+20</span>
                           </div>
                           <div data-lng="experience1b" class="year-details">
                              <p data-lng="experience1a">MEMBERS IN<br> OUR GROUP</p>
                           </div>
                        </div>
                        <div class="experience">
                           <div class="year">
                              <span>+50</span>
                           </div>
                           <div data-lng="experience2b" class="year-details">
                              <p data-lng="experience2a">IDEAS FOR<br> FUTURE PROJECTS</p>
                           </div>
                        </div>
                        <div class="experience">
                           <div class="year">
                              <span>+7</span>
                           </div>
                           <div data-lng="experience3b" class="year-details">
                              <p data-lng="experience3a" class="pad">YEARS OF<br> EXPERIENCE ON AVERAGE</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <p data-lng="about-objetive" class="about-footer" data-aos="fade-down">
               Our objective of bringing remunerable and sustainable products. As we keep on growing and supporting each other reaching self-improvement on each other skills.
            </p>
            <!--? Brand Shape  -->
            <div class="about-shape d-none d-xl-block">
               <img src="/assets/img/gallery/about_shape.png" alt="">
            </div>
         </section>
         <!-- Services Area End -->
         <!--? Blog Ara Start -->
         <section id="profiles" class="home-blog-area section-padding30">
            <div class="container">
               <div class="row">
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6" data-aos="zoom-in-left">
                     <div class="single-team mb-30">
                        <div class="team-img">
                           <img src="/assets/img/gallery/blog01.png" alt="">
                        </div>
                        <div class="team-caption">
                           <h3><a data-lng="captitle1" href="#">Web / videogames & assets / full-stack development</a></h3>
                           <p data-lng="capdesc1">Our members have experience with all those profiles in development. Facing to languages like .NET, Java, Python, C++, PHP, etc</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6" data-aos="zoom-in-down">
                     <div class="single-team mb-30">
                        <div class="team-img">
                           <img src="/assets/img/gallery/blog02.png" alt="">
                        </div>
                        <div class="team-caption">
                           <h3><a data-lng="captitle2" href="#">3D modeling / texturing / musical composition and general artworking</a></h3>
                           <p data-lng="captitle2">Some of our members also have experience on the artworking world. On this section we can see programs like Blender, Photoshop, FLStudio, etc</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6" data-aos="zoom-in-right">
                     <div class="single-team mb-30">
                        <div class="team-img">
                           <img src="/assets/img/gallery/blog03.png" alt="">
                        </div>
                        <div class="team-caption">
                           <h3><a data-lng="captitle3" href="#">Sysadmin / devops and other enterpreneur projects</a></h3>
                           <p data-lng="capdesc3">Finally, there are other profiles like sys-admin and dev-ops ones. There are people that doesn't meet any of the mentioned profiles, but also has other enterpreneur ideas.</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Services Ara End -->
         <!--? Gallery Area Start -->
         <!--- TODO: Show our projects here -->
         <!---
            <section class="gallery-area">
                <div class="container-fluid p-0">
                    <div class="row no-gutters">
                        <div class="col-xl-5 col-lg-4 col-md-6 col-sm-6">
                            <div class="gallery-box">
                                <div class="single-gallery">
                                    <div class="gallery-img" style="background-image: url(assets/img/gallery/gallery1.png);"></div>
                                    <div class="cap-icon">
                                        <a href="assets/img/gallery/gallery1.png" class="ti-fullscreen img-pop-up"></a>
                                    </div>
                                    <div class="g-caption">
                                        <h4>The Last man</h4>
                                        <p>Duis aute irure dolor in reprehenderit in voluptate velit.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                            <div class="gallery-box">
                                <div class="single-gallery">
                                    <div class="gallery-img" style="background-image: url(assets/img/gallery/gallery2.png);"></div>
                                    <div class="cap-icon">
                                        <a href="assets/img/gallery/gallery2.png" class="ti-fullscreen img-pop-up"></a>
                                    </div>
                                    <div class="g-caption">
                                        <h4>The Last man</h4>
                                        <p>Duis aute irure dolor in reprehenderit in voluptate velit.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                            <div class="gallery-box">
                                <div class="single-gallery">
                                    <div class="gallery-img" style="background-image: url(assets/img/gallery/gallery3.png);"></div>
                                    <div class="cap-icon">
                                        <a href="assets/img/gallery/gallery3.png" class="ti-fullscreen img-pop-up"></a>
                                    </div>
                                    <div class="g-caption">
                                        <h4>The Last man</h4>
                                        <p>Duis aute irure dolor in reprehenderit in voluptate velit.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-4 col-md-6 col-sm-6">
                            <div class="gallery-box">
                                <div class="single-gallery">
                                    <div class="gallery-img" style="background-image: url(assets/img/gallery/gallery4.png);"></div>
                                    <div class="cap-icon">
                                        <a href="assets/img/gallery/gallery4.png" class="ti-fullscreen img-pop-up"></a>
                                    </div>
                                    <div class="g-caption">
                                        <h4>The Last man</h4>
                                        <p>Duis aute irure dolor in reprehenderit in voluptate velit.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                            <div class="gallery-box">
                                <div class="single-gallery">
                                    <div class="gallery-img" style="background-image: url(assets/img/gallery/gallery5.png);"></div>
                                    <div class="cap-icon">
                                        <a href="assets/img/gallery/gallery5.png" class="ti-fullscreen img-pop-up"></a>
                                    </div>
                                    <div class="g-caption">
                                        <h4>The Last man</h4>
                                        <p>Duis aute irure dolor in reprehenderit in voluptate velit.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                            <div class="gallery-box">
                                <div class="single-gallery">
                                    <div class="gallery-img" style="background-image: url(assets/img/gallery/gallery6.png);"></div>
                                    <div class="cap-icon">
                                        <a href="assets/img/gallery/gallery6.png" class="ti-fullscreen img-pop-up"></a>
                                    </div>
                                    <div class="g-caption">
                                        <h4>The Last man</h4>
                                        <p>Duis aute irure dolor in reprehenderit in voluptate velit.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            -->
         <!-- Gallery Area End -->
         <!--? Brand Area Start -->
         <section id="team" class="brand-area pb-bottom section-padding30">
            <div class="container">
               <div class="row">
                  <div class="col-lg-6" data-aos="flip-up">
                     <!-- Section Tittle -->
                     <div class="section-tittle mb-30">
                        <h2 data-lng="take-a-look">Take a look <br>at our team</h2>
                        <p data-lng="we-are-a-team">We are a team of +20 people. Just check it!</p>
                        <!--- TODO
                           <a href="#" class="btn">Start</a>
                           -->
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-6" data-aos="flip-up">
                     <div class="single-brand text-center mb-30">
                        <img src="/assets/img/team/1.png" alt="">
                        <p class="name">Álvaro - z3nth10n</p>
                        <p>CEO</p>
                        <p data-lng="profile-z3nth10n">Developer: C#, web / sysadmin</p>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-6" data-aos="flip-up">
                     <div class="single-brand text-center mb-30">
                        <img src="/assets/img/team/2.png" alt="">
                        <p class="name">Felipe - Fenex</p>
                        <p data-lng="rol-fenex">Co-founder of Lerp2Dev Team</p>
                        <p data-lng="profile-fenex">Unity3D Developer / 3D Modeling / Texturing / Animations</p>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-6" data-aos="flip-up">
                     <div class="single-brand text-center mb-30">
                        <img src="/assets/img/team/3.png" alt="">
                        <p class="name">Nico - Nightmare</p>
                        <p data-lng="rol-nico">Active contributor of Lerp2Dev Team</p>
                        <p data-lng="profile-nico">3D Modeling / Texturing</p>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-6" data-aos="flip-up">
                     <div class="single-brand text-center mb-30">
                        <img src="/assets/img/team/4.png" alt="">
                        <p class="name">Joel - XXLuigiMario</p>
                        <p data-lng="rol-joel">Co-founder of SykoLand (2015 project)</p>
                        <p data-lng="profile-joel">Developer: Java & Python</p>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-6" data-aos="flip-up">
                     <div class="single-brand text-center mb-30">
                        <img src="/assets/img/team/5.png" alt="">
                        <p class="name">Bernat - Bernatixer</p>
                        <p data-lng="rol-berni">Contributor / 2013</p>
                        <p data-lng="profile-berni">Developer: Java</p>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-6" data-aos="flip-up">
                     <div class="single-brand text-center mb-30">
                        <img src="/assets/img/team/6.png" alt="">
                        <p class="name">David Martínez Ros</p>
                        <p data-lng="rol-david">Contributor</p>
                        <p data-lng="profile-david">Developer: Java & C++</p>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-6" data-aos="flip-up">
                     <div class="single-brand text-center mb-30">
                        <img src="/assets/img/team/7.png" alt="">
                        <p class="name">Edu</p>
                        <p data-lng="rol-edu">Contributor</p>
                        <p data-lng="profile-edu">Sysadmin</p>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-6" data-aos="flip-up">
                     <div class="single-brand text-center mb-30">
                        <img src="/assets/img/team/8.png" alt="">
                        <p class="name">Asier - Abderramah</p>
                        <p data-lng="rol-abde">Contributor / ElHacker.Net 2011</p>
                        <p data-lng="profile-abde">Developer: VB.NET</p>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-6" data-aos="flip-up">
                     <div class="single-brand text-center mb-30">
                        <img src="/assets/img/team/9.png" alt="">
                        <p class="name">Andrei</p>
                        <p data-lng="rol-andrei">Contributor / Classmate</p>
                        <p data-lng="profile-andrei">Developer</p>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-6" data-aos="flip-up">
                     <div class="single-brand text-center mb-30">
                        <img src="/assets/img/team/10.png" alt="">
                        <p class="name">Gabriel - Gajosu</p>
                        <p data-lng="rol-gajosu">Lerp2Dev Contributor / Spirate Member 2011 and CEO of a social network project</p>
                        <p data-lng="profile-gajosu">Web developer</p>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-6" data-aos="flip-up">
                     <div class="single-brand text-center mb-30">
                        <img src="/assets/img/team/11.png" alt="">
                        <p class="name">Dani - dby_x86</p>
                        <p data-lng="rol-wissen">Contributor / 2013</p>
                        <p data-lng="profile-wissen">Developer: LUA</p>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-6" data-aos="flip-up">
                     <div class="single-brand text-center mb-30">
                        <img src="/assets/img/team/12.png" alt="">
                        <p class="name">Fabian - neoN</p>
                        <p data-lng="rol-neon">Contributor / 2020</p>
                        <p data-lng="profile-neon">Unreal Engine developer</p>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-6" data-aos="flip-up">
                     <div class="single-brand text-center mb-30">
                        <img src="/assets/img/team/more.png" alt="">
                        <p data-lng="anon-people" class="name">+8 people who prefers to keep the anonymity of their identity</p>
                     </div>
                  </div>
               </div>
            </div>
            <!--? Brand Shape  -->
            <div class="brand-shape d-none d-lg-block">
               <img src="/assets/img/gallery/brand_shape.png" alt="">
            </div>
         </section>
         <!-- Brand Area End -->  
         <!--? Testimonial Start -->
         <div id="join-us" class="testimonial-area testimonial-padding">
            <div class="container ">
               <div class="row d-flex justify-content-center">
                  <div class="col-xl-11 col-lg-11 col-md-9">
                     <div class="h1-testimonial-active">
                        <!-- Single Testimonial -->
                        <div class="single-testimonial text-center">
                           <!-- Testimonial Content -->
                           <div class="testimonial-caption">
                              <div class="testimonial-top-cap" data-aos="fade-left">
                                 <img src="/assets/img/gallery/testimonial.png" alt="">
                                 <p data-lng="want-to-join">Would you like to join us? Please, contact us by filling the following form.</p>
                              </div>
                              <!-- founder -->
                              <div class="testimonial-founder" data-aos="zoom-in">
                                 <div class="d-flex align-items-center justify-content-center">
                                    <div class="founder-img">
                                       <img src="/assets/img/gallery/Homepage_testi.png" alt="">
                                    </div>
                                    <div class="founder-text">
                                       <span data-lng="vizzuta-team">VIZZUTA Team</span>
                                       <p data-lng="waiting-for-you">Is waiting for you!</p>
                                    </div>
                                 </div>
                                 <span class="welcome-btn">
                                 <a data-lng="welcome-form" href="#" class="btn" data-form="welcome">Welcome form</a>
                                 </span>
                              </div>
                              <br><br>
                              <div data-aos="fade-right">
                                 <p class="info1" data-lng="join-discord">Or join our Discord by clicking on this logo</p>
                                 <div class="discord-logo">
                                    <a class="discord-img" href="https://discord.gg/ju2qDtM" target="_blank"><img src="/assets/img/gallery/discord.png" alt=""></a>
                                    <a class="discord-gif" href="https://discord.gg/ju2qDtM" target="_blank"><img src="/assets/img/gallery/discord.gif" alt=""></a>
                                 </div>
                                 <br>
                                 <img class="discord-badge" src="https://img.shields.io/discord/479096180601782274.svg" alt="">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         </div>
         <!--- Services Start-->
         <section id="services" class="blogs-area section-padding30">
            <div class="container">
               <div class="row justify-content-center">
                  <div class="col-lg-6">
                     <!-- Section Tittle -->
                     <div class="section-tittle text-center mb-30">
                        <h2 data-lng="services">Services</h2>
                        <p data-lng="all-our-services">All our services</p>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-4 col-md-6" data-aos="zoom-in-left">
                     <div class="single-blogs mb-100">
                        <div class="blog-img">
                           <img src="/assets/img/gallery/blog1.png" alt="">
                        </div>
                        <div class="blog-cap">
                           <span class="color1" data-lng="wip">WORK IN PROGRESS</span>
                           <h4><a href="blog_details.html" data-lng="blog1">VIZZUTA STORE - A store where we sell our creations.</a></h4>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-4 col-md-6" data-aos="zoom-in-down">
                     <div class="single-blogs mb-100">
                        <div class="blog-img">
                           <img src="/assets/img/gallery/blog2.png" alt="">
                        </div>
                        <div class="blog-cap">
                           <span class="color1" data-lng="wip">WORK IN PROGRESS</span>
                           <h4><a href="blog_details.html" data-lng="blog2">VIZZUTA SERVERS - Specific featured dedicated servers focused for clients with no sys-admin knowledge.</a></h4>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-4 col-md-6" data-aos="zoom-in-right">
                     <div class="single-blogs mb-100">
                        <div class="blog-img">
                           <img src="/assets/img/gallery/blog3.png" alt="">
                        </div>
                        <div class="blog-cap">
                           <span class="color1" data-lng="future-services">Future services</span>
                           <h4><a href="blog_details.html" data-lng="blog3">More services and features will come on the near future. Keep in track!</a></h4>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--? Brand Shape  -->
            <div class="blog-shape d-none d-xl-block">
               <img src="/assets/img/gallery/blog_shape.png" alt="">
            </div>
         </section>
         <!--- Services End -->
      </main>
      <footer>
         <!--? Footer Start-->
         <div class="footer-area footer-bg">
            <div class="container">
               <div class="footer-top footer-padding">
                  <div class="row d-flex justify-content-between">
                     <div class="col-xl-4 col-lg-4 col-md-5 col-sm-8">
                        <div class="single-footer-caption mb-50">
                           <!-- logo -->
                           <div class="footer-logo">
                              <a href="index.html"><img src="/assets/img/logo/logo2_footer.png" alt=""></a>
                           </div>
                           <div class="footer-tittle">
                              <div class="footer-pera">
                                 <p class="info1" data-lng="thanks">Thanks for your visit!<br/>We hope to see you again!</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-xl-2 col-lg-2 col-md-5 col-sm-6">
                        <div class="single-footer-caption mb-50">
                           <div class="footer-tittle">
                              <h4>Navigation</h4>
                              <ul>
                                 <li><a href="#" data-lng="home">Home</a></li>
                                 <li><a href="#" data-lng="events">Events (WIP)</a></li>
                                 <li><a href="#" data-lng="contact-us">Contact us (WIP)</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-xl-2 col-lg-2 col-md-5 col-sm-6">
                        <div class="single-footer-caption mb-50">
                           <div class="footer-tittle">
                              <h4>Useful Links</h4>
                              <ul>
                                 <li><a href="#" data-lng="registration">Registration (WIP)</a></li>
                                 <li><a href="#" data-lng="login">Login (WIP)</a></li>
                                 <li><a href="#" data-lng="policy">Policy (WIP)</a></li>
                                 <li><a href="#" data-lng="terms-n-conditions">Terms & Conditions (WIP)</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <!-- Instagram -->
                     <div class="col-xl-4 col-lg-4 col-md-5 col-sm-7">
                        <div class="single-footer-caption mb-50">
                           <div class="footer-tittle">
                              <h4>SPONSORS</h4>
                           </div>
                           <div class="instagram-gellay">
                              <h3>Would you like to participate in our sponsor program?</h3>
                              <p class="info1">Please, contact us via our sponsor form!</p>
                              <a href="#" class="btn" data-form="sponsors">Sponsor form</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="footer-bottom">
                  <div class="row d-flex justify-content-between align-items-center">
                     <div class="col-xl-9 col-lg-8">
                        <div class="footer-copy-right">
                           <p data-lng="copyright">
                              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                              Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a> and modified by <a href="https://z3nth10n.net/">z3nth10n</a>
                              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                           </p>
                        </div>
                     </div>
                     <div class="col-xl-3 col-lg-4">
                        <!-- Footer Social -->
                        <div class="footer-social f-right">
                           <span data-lng="follow-us">Follow Us</span>
                           <a href="https://discord.gg/ju2qDtM"><i class="fab fa-discord"></i></a>
                           <!--- TODO: Social networks
                              <a href="https://www.facebook.com/sai4ull"><i class="fab fa-facebook-f"></i></a>
                              <a href="#"><i class="fas fa-globe"></i></a>
                              <a href="#"><i class="fab fa-instagram"></i></a>
                              -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Footer End-->
      </footer>
      <!-- Scroll Up -->
      <div id="back-top" >
         <a title="Go to Top" href="#"> <i class="fas fa-level-up-alt"></i></a>
      </div>
      <!--- Modal Windows Start -->
      <div class="modal fade" id="contact-window" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Contact us</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <form id="contact">
                  <div class="modal-body row">
                     <!--- Contact Body Start -->
                     <div class="col-sm-6">
                        <div class="form-group">
                           <input class="form-control valid" name="name" id="name" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'" placeholder="Enter your name" required>
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           <input class="form-control valid" name="email" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'" placeholder="Enter email address" required>
                        </div>
                     </div>
                     <div class="col-12">
                        <div class="form-group">
                           <input class="form-control" name="subject" id="subject" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Subject'" placeholder="Enter Subject" required>
                        </div>
                     </div>
                     <div class="col-12 default-select">
                        <select name="reason" id="contact-reason">
                           <option selected value="0">I want to contact you</option>
                           <option value="1">I want to join our group</option>
                           <option value="2">I want to be sponsored on your website</option>
                        </select>
                        <br>
                     </div>
                  </div>
                  <div id="reason_0" class="modal-body row">
                     <div class="col-12">
                        <div class="form-group">
                           <textarea class="form-control w-100" name="message" id="message" cols="30" rows="9" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Message'" placeholder="Enter Message" required></textarea>
                        </div>
                     </div>
                  </div>
                  <div id="reason_1" class="modal-body row">
                     <!--- Age, profile (knowledge) and description of you and what would you apport to the group -->
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label for="age">Age</label>
                           <input class="form-control valid" name="age" id="age" type="date"> <!--- Require when select -->
                        </div>
                     </div>
                     <div class="col-sm-6 prelative">
                        <div class="form-group push-to-bottom">
                           <input class="form-control valid" name="mobile_phone" id="mobile_phone" type="tel" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your mobile phone'" placeholder="Enter your mobile phone">
                        </div>
                     </div>
                     <div class="col-12">
                        <div class="form-group">
                           <textarea class="form-control w-100" name="about_you" id="about_you" cols="30" rows="9" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Describe your knowledge (your profile), a short introduction about you and in what aspects could you contribute to this group.'" placeholder="Describe your knowledge (your profile), a short introduction about you and in what aspects could you contribute to this group."></textarea>
                        </div>
                     </div>
                  </div>
                  <div id="reason_2" class="modal-body row">
                     <!--- Website -->
                     <div class="col-12">
                        <div class="form-group">
                           <input class="form-control" name="website" id="website" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Website'" placeholder="Enter Website: https://example.com"
                              pattern="[Hh][Tt][Tt][Pp][Ss]?:\/\/(?:(?:[a-zA-Z\u00a1-\uffff0-9]+-?)*[a-zA-Z\u00a1-\uffff0-9]+)(?:\.(?:[a-zA-Z\u00a1-\uffff0-9]+-?)*[a-zA-Z\u00a1-\uffff0-9]+)*(?:\.(?:[a-zA-Z\u00a1-\uffff]{2,}))(?::\d{2,5})?(?:\/[^\s]*)?">
                        </div>
                     </div>
                     <p>
                        Remember that we need to put in contact with you!
                     </p>
                     <p>
                        Sponsors must be reciprocal, on both websites.
                     </p>
                  </div>
                  <!---
                     TODO
                     <div class="switch-wrap d-flex justify-content-between">
                     <p>I would like to create an account on this website</p>
                     <div class="primary-switch">
                     <input type="checkbox" id="primary-switch" checked="">
                     <label for="primary-switch"></label>
                     </div>
                     </div>
                     -->           
                  <!--- Contact Body End -->
                  <div class="modal-footer">
                     <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                     <button type="submit" class="btn btn-primary">Send</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <!--- Modal Windows End -->  
      <!-- JS here -->
      <script src="/assets/js/vendor/modernizr-3.5.0.min.js"></script>
      <!-- Jquery, Popper, Bootstrap -->
      <script src="/assets/js/vendor/jquery-1.12.4.min.js"></script>
      <script src="/assets/js/popper.min.js"></script>
      <script src="/assets/js/bootstrap.min.js"></script>
      <!-- Jquery Mobile Menu -->
      <script src="/assets/js/jquery.slicknav.min.js"></script>
      <!-- Jquery Slick , Owl-Carousel Plugins -->
      <script src="/assets/js/owl.carousel.min.js"></script>
      <script src="/assets/js/slick.min.js"></script>
      <!-- One Page, Animated-HeadLin -->
      <script src="/assets/js/wow.min.js"></script>
      <script src="/assets/js/animated.headline.js"></script>
      <script src="/assets/js/jquery.magnific-popup.js"></script>
      <!-- Nice-select, sticky -->
      <script src="/assets/js/jquery.nice-select.min.js"></script>
      <script src="/assets/js/jquery.sticky.js"></script>
      <!-- contact js -->
      <script src="/assets/js/contact.js"></script>
      <script src="/assets/js/jquery.form.js"></script>
      <script src="/assets/js/jquery.validate.min.js"></script>
      <script src="/assets/js/mail-script.js"></script>
      <script src="/assets/js/jquery.ajaxchimp.min.js"></script>
      <!-- Jquery Plugins, main Jquery -->	
      <script src="/assets/js/plugins.js"></script>
      <script src="/assets/js/main.js"></script>
      <!--- APP -->
      <script src="/assets/js/app.js"></script>
      <script src="/assets/js/router.js"></script>
      <!--- AOS -->
      <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
      <script>AOS.init();</script>
   </body>
</html>